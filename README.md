# README #



### What is this repository for? ###

This script Monitors a specified application.
It detects whether the app is running, starts it up if it isn't and makes sure it stays the active window. 
* Version 0.2


### How do I get set up? ###

* Specify the application to be monitored in source document. Compile using AutoHotKey and run exe. 
* Configuration: Specify application to be monitored in APCKioskWD.ahk
* Dependencies: Windows 7/8.1, AutoHotKey.exe
* Deployment instructions: Initially written to monitor APCKiosk.exe but can monitor any basic app. 

### Contribution guidelines ###

Get in touch with Owner

### Who do I talk to? ###

Andre Louw <alouw@apcglobal.com>