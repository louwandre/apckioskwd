;
; APCKioskWD Version: 		0.2
; Language:       		AutoHotkey
; Platform:       		Windows7/8.1
; Author:         		Andre Louw <alouw@apcglobal.com>
;
; Script Function:
;	- This scripts Monitors a Xibo Player
;	- First you specify the app to be monitored
;	- It start by killing all processes with the same name to ensure no duplicate processes are running 
;	- Next it checks to see if the specified app is running. 
;		- If the app is not running and exists it wills tart it up
;		- If the app is not running and can't be found a warning message will be shown
;	- If app is running it makes sure that app stays the active window 
;	- This script is then looped every 2 seconds

;################ Specify Process to be monitored  ##############

ProcessName = APCKiosk.exe								;Kiosk name
ProcessNameString := "APCKiosk.exe"						;Process name as string
ProcessPath =  %A_ScriptDir%\APCKiosk.exe		;Path where Kiosk needs to be started from

;################ Initialize Query ###############

ProcessNameQuery := "Select * from Win32_Process  where name = '" ProcessNameString "'"

;sleep 10000	;sleep 10 seconds

;########## Fresh start (Kill all apps and create new app instance) ############

Gosub FreshStart

;################ Loop Script (Monitors App)################

Loop
{
Gosub KillDuplicates
;/*------ Check to see if app is running ------*/
	Process, Exist, %ProcessName% 
		{
			If (ErrorLevel = 0) 								; If it is not running
				{
					Gosub Startapp
				}
			else if (A_Min = 00) 						; is time x:00 ?
				{
					Gosub RestartApp	
				}
			else										
			{
				Gosub MakeActive															
			}
			
		}
;/*------ Loop interval ------*/
	sleep 2000																				;Loop every 2 seconds	
}

;################ Functions ################
FreshStart:
	count=0					
	for process in ComObjGet("winmgmts:").ExecQuery("Select * from Win32_Process")
	{
		if	(process.name = ProcessNameString )
			count++
	}	

	if count > 0
	{
		for process in ComObjGet("winmgmts:").ExecQuery("Select * from Win32_Process  where name = '" ProcessNameString "'")
		{
			process, close, % process.ProcessId	
		}
		
		sleep 1000
		
		IfExist, %ProcessPath%							; Check if App exists in path specified
		{
			Run,%ProcessPath%, , , ActivePID			;Start app from specified path and record PID
			
		}
		else
		{
			MsgBox Program does not exist `nLooking for : %ProcessPath%			;Raise error if app can't be found

		}
	}
	return

KillDuplicates:
	count=0					
	for process in ComObjGet("winmgmts:").ExecQuery("Select * from Win32_Process")
	{
		if	(process.name = ProcessNameString )
			count++
	}	
	
	if (count > 1)
	{
		ActiveProcessID := ActivePID 
		for process in ComObjGet("winmgmts:").ExecQuery("Select * from Win32_Process  where name = '" ProcessNameString "'")
		{
			processID := process.ProcessId
			if (processID != ActiveProcessID)
				{
					process, close, % process.ProcessId		
				}
		}
	}
	return

Startapp:
	IfExist, %ProcessPath%						; Check if App exists in path specified
	{
		Run,%ProcessPath%, , , ActivePID			;Start app from specified path and record PID
		
	}
	else
	{
		MsgBox Program does not exist `nLooking for : %ProcessPath%			;Raise error if app can't be found
		Exitapp
	}
	return
	
RestartApp:
	{
		process, close, %ActivePID%
		sleep 1000
		
		;/*------- Check to see if all processes are stopped
		count=0					
		for process in ComObjGet("winmgmts:").ExecQuery("Select * from Win32_Process")
		{
			if	(process.name = ProcessNameString )
				count++
		}	
					
		if count > 0
		{
			for process in ComObjGet("winmgmts:").ExecQuery("Select * from Win32_Process  where name = '" ProcessNameString "'")
			{
				process, close, % process.ProcessId	
			}
			
			sleep 1000
			
			IfExist, %ProcessPath%							; Check if App exists in path specified
			{
				Run,%ProcessPath%, , , ActivePID			;Start app from specified path and record PID
				
			}
			else
			{
				MsgBox Program does not exist `nLooking for : %ProcessPath%			;Raise error if app can't be found
				Exitapp
			}
		}
		else
		{
			IfExist, %ProcessPath%							; Check if App exists in path specified
			{
				Run,%ProcessPath%, , , ActivePID			;Start app from specified path and record PID
				
			}
			else
			{
				MsgBox Program does not exist `nLooking for : %ProcessPath%			;Raise error if app can't be found
				Exitapp
			}
		}
		sleep 60000	
	}
	return

MakeActive:
	IfWinActive, ahk_pid %ActivePID% 					;If App is active do nothing
	{				
		sleep 1000	
	}
	else
	{
		WinActivate, ahk_pid %ActivePID%										;Make app active if it is not 
	}	
	return
	
;################ Action Buttons ################

#p::Pause  																					;Press WindowsKey+P to pause.Press it again to resume.

